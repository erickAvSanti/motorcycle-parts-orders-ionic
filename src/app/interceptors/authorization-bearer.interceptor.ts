import { Injectable, isDevMode } from '@angular/core'
import { Storage } from '@ionic/storage'
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { from, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Injectable()
export class AuthorizationBearerInterceptor implements HttpInterceptor {

  constructor(private storage: Storage) {}

  async get_token_props(){
    const token = this.storage.get('token')
    const token_type = this.storage.get('token_type')
    const values = await Promise.all([ token_type, token ])
    return values
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const values = this.get_token_props()
    if( values ){
      return from(values).pipe(
        switchMap( values =>{
          if(values){
            const cloned = request.clone({
              headers: request.headers.set('Authorization', `${ values[0] } ${ values[1] }`)
            })
            if( isDevMode() )console.log(`headers: ${JSON.stringify(cloned)}`)
            return next.handle(cloned)
          }else{
            return next.handle(request)
          }
        })
      )
    }
    return next.handle(request)
  }
}
