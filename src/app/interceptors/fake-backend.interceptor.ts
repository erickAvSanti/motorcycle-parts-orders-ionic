import { Injectable, isDevMode } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import { Storage } from '@ionic/storage'
import { AuthUser } from '../authentication/auth-user';

// array in local storage for registered users

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
    private users: Array<AuthUser> = [];
    constructor(private storage: Storage){
        this.storage.get('users')
        .then( (data: any) => {
            const parse = JSON.parse(data) || []
            this.users = parse
        }).catch(err => {
            if( isDevMode() ) console.log(err)
            this.users = []
        })
    }
    private randomInt = (min: number, max: number): number => {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };
    fillWithDefaultUser(): void{
        const arr = this.users.filter( (user)=> user.email == 'test12345')
        if(arr.length == 0){
            this.users.push({id: this.randomInt(1,10), email:'test12345', password:'test12345'})
        }
    }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const { url, method, headers, body } = request;
        this.fillWithDefaultUser()
        let users = this.users
        const storage = this.storage
        // wrap in delayed observable to simulate server api call

        function handleRoute() {
            switch (true) {
                case url.endsWith(`/authenticate`) && method === 'POST':
                    return authenticate();
                case url.endsWith(`/users/register`) && method === 'POST':
                    return register();
                case url.endsWith(`/users`) && method === 'GET':
                    return getUsers();
                case url.match(/\/users\/\d+$/) && method === 'DELETE':
                    return deleteUser();
                default:
                    // pass through any requests not handled above
                    return next.handle(request);
            }    
        }

        // route functions

        function authenticate() {
            const { email, password } = body;
            const user = users.find(x => x.email === email && x.password === password);
            if (!user) return error('email or password is incorrect');
            return ok({
                id: user.id,
                email: user.email,
                firstname: user.firstname,
                lastname: user.lastname,
                token: 'fake-jwt-token',
                expires_in: 4,
            })
        }

        function register() {
            const user = body

            if (users.find(x => x.email === user.email)) {
                return error('email "' + user.email + '" is already taken')
            }

            user.id = users.length ? Math.max(...users.map(x => x.id)) + 1 : 1;
            users.push(user);
            storage.set('users', JSON.stringify(users));

            return ok();
        }

        function getUsers() {
            if (!isLoggedIn()) return unauthorized();
            return ok(users);
        }

        function deleteUser() {
            if (!isLoggedIn()) return unauthorized();

            users = users.filter(x => x.id !== idFromUrl());
            storage.set('users', JSON.stringify(users));
            return ok();
        }

        // helper functions

        function ok(body?) {
            return of(new HttpResponse({ status: 200, body }))
        }

        function error(message) {
            return throwError({ error: { message } });
        }

        function unauthorized() {
            return throwError({ status: 401, error: { message: 'Unauthorised' } });
        }

        function isLoggedIn() {
            return headers.get('Authorization') === 'Bearer fake-jwt-token';
        }

        function idFromUrl() {
            const urlParts = url.split('/');
            return parseInt(urlParts[urlParts.length - 1]);
        }
        return of(null)
            .pipe(mergeMap(handleRoute))
            .pipe(materialize()) // call materialize and dematerialize to ensure delay even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648)
            .pipe(delay(2000))
            .pipe(dematerialize());
    }
}

export const fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};