import { TestBed } from '@angular/core/testing';

import { AuthorizationBearerInterceptor } from './authorization-bearer.interceptor';

describe('AuthorizationBearerInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
        AuthorizationBearerInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: AuthorizationBearerInterceptor = TestBed.inject(AuthorizationBearerInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
