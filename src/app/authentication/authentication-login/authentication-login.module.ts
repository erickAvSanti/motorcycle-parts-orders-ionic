import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AuthenticationLoginPageRoutingModule } from './authentication-login-routing.module';

import { AuthenticationLoginPage } from './authentication-login.page';
import { ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from '../../material.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    MaterialModule,
    AuthenticationLoginPageRoutingModule
  ],
  exports:[
    MaterialModule,
  ],
  declarations: [AuthenticationLoginPage]
})
export class AuthenticationLoginPageModule {}
