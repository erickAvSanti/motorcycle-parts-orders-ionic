import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthenticationLoginPage } from './authentication-login.page';

const routes: Routes = [
  {
    path: '',
    component: AuthenticationLoginPage
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthenticationLoginPageRoutingModule {}
