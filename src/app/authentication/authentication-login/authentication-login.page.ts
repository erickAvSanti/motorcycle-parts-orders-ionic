import { Component, isDevMode, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { catchError, finalize, tap } from 'rxjs/operators';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-authentication-login',
  templateUrl: './authentication-login.page.html',
  styleUrls: ['./authentication-login.page.scss'],
})
export class AuthenticationLoginPage implements OnInit {
  form: FormGroup = new FormGroup({
    username: new FormControl('',[ Validators.required, Validators.minLength(8) ]),
    password: new FormControl('',[ Validators.required, Validators.minLength(8) ]),
  })
  sync_login: {wait: boolean} = {wait: false}
  hidePassword = true

  constructor( 
    private authService: AuthService,
    private router: Router,
   ) { }

  ngOnInit() {
    if(this.authService.isLoggedIn())this.router.navigate(['/dashboard'])
  }

  get username(){
    return this.form.get('username')
  }

  get password(){
    return this.form.get('password')
  }

  login(): void{
    if( this.sync_login.wait )return 
    this.sync_login.wait = true
    this.authService.login({email: this.username.value, password: this.password.value})
    .pipe(finalize( () => this.sync_login.wait = false ))
    .subscribe(
      res => {
        if(isDevMode())console.log(res)
        if(!this.authService.tryToRedirectUrl()) this.router.navigate(['dashboard'])
      },
      err => {
        if(isDevMode())console.log(err)
      }
    )
  }

  getUsernameErrorMessage(): string{
    if( this.username.hasError('required') ) return 'usuario requerido';
    if( this.username.hasError('minlength') ) return 'mínimo 8 caracteres';
    return ''
  }

  getPasswordErrorMessage(): string{
    if( this.password.hasError('required') ) return 'contraseña requerido';
    if( this.password.hasError('minlength') ) return 'mínimo 8 caracteres';
    return ''
  }

}
