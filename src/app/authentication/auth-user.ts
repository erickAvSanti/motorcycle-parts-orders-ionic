import { Auth } from './auth';

export interface AuthUser extends Auth{
    id?: number
    firstname?: string
    lastname?: string
    created_at?: string
    updated_at?: string
    token?: string
    token_type?: string
    expires_in?: number
}
