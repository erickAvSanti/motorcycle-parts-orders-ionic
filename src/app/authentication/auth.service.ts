import { Injectable, isDevMode } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { shareReplay, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { AuthUser } from './auth-user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private defaultHttpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept' : 'application/json'
    })
  }
  public redirectUrl: string

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private router: Router,
  ) { }
  login(auth: AuthUser){
    return this.http.post<AuthUser>(`${environment.SERVER_URL_API}/auth/login`,auth,this.defaultHttpOptions)
    .pipe(
      tap(res => this.setSession(res)),
      shareReplay()
    ) 
  }
  private async setSession(authResult: AuthUser ){
    if( isDevMode() ){
      console.log(`authResult: `)
      console.log(authResult)
    }
    const expires_at = moment().utc().add(authResult.expires_in,'hour')
    if( isDevMode() ){
      console.log(expires_at.format('DD/MM/YYYY HH:mm:ss a'))
    }
    await this.storage.set('token',authResult.token)
    await this.storage.set('token_type',authResult.token_type)
    await this.storage.set('expires_at',JSON.stringify(expires_at.valueOf()))
  }

  async logout() {
    await this.storage.remove("token");
    await this.storage.remove("token_type");
    await this.storage.remove("expires_at");
    this.router.navigate(['/auth/login'])
  }

  async isLoggedIn() {
    return moment().utc().isBefore(await this.getExpiration())
  }

  isLoggedOut() {
    return !this.isLoggedIn()
  }

  async getExpiration() {
    const expiration = await this.storage.get("expires_at")
    const expires_at = JSON.parse(expiration)
    return moment(expires_at)
  }    
  tryToRedirectUrl():boolean{
    if(this.redirectUrl){
      
      /*
      const navigationExtras: NavigationExtras = {
        queryParamsHandling: 'preserve',
        preserveFragment: true
      }
      this.router.navigate([this.redirectUrl], navigationExtras)
      */
      this.router.navigate([this.redirectUrl])
      this.redirectUrl = null
      return true
    }
    return false
  }
}
