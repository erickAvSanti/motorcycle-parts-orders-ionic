import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonRefresher, ModalController } from '@ionic/angular';
import { finalize } from 'rxjs/operators';
import { AuthService } from '../authentication/auth.service';
import { OrderAddComponent } from './order-add/order-add.component';
import { OrderService } from './order.service';
import * as moment from 'moment';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {

  items: Array<any> = []
  orders_info: {wait_list: boolean, records: Array<any>} = {wait_list: false, records: []}
  flag = false
  @ViewChild(IonRefresher) ionRefresher: IonRefresher
  constructor(
    public modalController: ModalController,
    private authService: AuthService,
    private router: Router,
    private orderService: OrderService,
  ) { }

  ngOnInit() {
    if(this.authService.isLoggedOut()){
      this.router.navigate(['/auth/login'])
    }
    this.items = [
      {
        code:345,
        created_at: '20/20/2020 02:02:02',
        prod_name: 'Pistón'
      }
    ]
    this.getOrders()
  }
  getOrders(){
    if(this.orders_info.wait_list)return
    this.orders_info.wait_list = true
    this.orderService.all()
    .pipe(finalize( () => {
      this.orders_info.wait_list = false
      this.flag = true
      this.ionRefresher.complete()
    }))
    .subscribe(
      next => this.addExtraPropsToOrdersInfo(next),
      error => console.log(error),
      () => {}
    )
  }
  addExtraPropsToOrdersInfo(data){
    data.records.forEach(element => {
      element.show_products = false
    })
    this.orders_info = data
    console.log(data)
  }
  openAddModal(){
    this.presentModal()
  }
  async presentModal() {
    const modal = await this.modalController.create({
      component: OrderAddComponent,
      cssClass: 'my-custom-class',
      swipeToClose: true,
    })
    return await modal.present()
  }
  doRefresh(event) {
    this.getOrders()
  }
  transformIssuedAtDate(issued_at_date_str){
    return moment(issued_at_date_str.slice(0,10)).format('DD/MM/YYYY')
  }

}
