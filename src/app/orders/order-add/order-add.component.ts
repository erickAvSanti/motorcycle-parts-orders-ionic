
import { AfterViewInit, Component, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { AlertController, IonDatetime, LoadingController, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { OrderService } from '../order.service';
import { ProductSearchComponent } from '../product-search/product-search.component';
import * as moment from 'moment';
import { from, timer } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-order-add',
  templateUrl: './order-add.component.html',
  styleUrls: ['./order-add.component.scss'],
})
export class OrderAddComponent implements OnInit, OnDestroy, AfterViewInit {

  products: Array<any> = []
  modal_product_search: {wait: boolean} = {wait: false}
  issued_at = moment()
  processing_order = false

  minIssuedDate = moment().add(-1,'year')
  maxIssuedDate = moment().add(1,'year')

  @ViewChildren(IonDatetime) ionDatetimes: QueryList<IonDatetime>

  constructor(
    public modalController: ModalController,
    private orderService: OrderService,
    private alertController: AlertController,
    private loadingController: LoadingController,
    private storage: Storage,
  ) { }
  

  ngOnInit() {
    this.storage.get('current_order_products')
    .then( _ => this.products = JSON.parse(_) || [])
    this.orderService.currentOrder$.subscribe(
      next => {
        if(next.product && next.product.id){
          const found = this.products.find( prod => prod.id == next.product.id)
          if(!found){
            this.products.push(next.product)
          }
        }        
      },
      err => {
        console.log(err)
      },
      () => {}
    )
  }
  ngAfterViewInit(){
    this.ionDatetimes.forEach(element => {
      element.ionChange.subscribe(
        _ =>{
          if(element.name == 'issued_at'){
            this.issued_at = moment(element.value.slice(0,10))
          }
        }
      )
    })
  }
  ngOnDestroy(){
    this.storageProducts()
  }
  storageProducts(): void{
    this.storage.set('current_order_products',JSON.stringify(this.products))
  }
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }
  addProduct(){
    this.presentModal()
  }
  
  async presentModal() {
    /*
    from(
      this.modalController.create({
        component: ProductSearchComponent,
        cssClass: 'my-custom-class',
        swipeToClose: true,
      })
    )
    .subscribe(
      modal => {
        from(modal.present()).subscribe(
          result => console.log(result)
        )
      }
    )
    */
    if(this.modal_product_search.wait)return
    this.modal_product_search.wait = true
    const modal = await this.modalController.create({
      component: ProductSearchComponent,
      cssClass: 'my-custom-class',
      swipeToClose: true,
    })
    await modal.present().finally( () => this.modal_product_search.wait = false)
    
  }
  async confirmOrder(){
    if(this.processing_order)return
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Orden de pedidos',
      message: `Registrar pedido ?`,
      buttons: [
        {
          text: 'NO',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: 'SI',
          handler: () => {
            this.processing_order = true
            const loading = this.showProcessOrderLoading()
            loading.then(
              loading => {
                this.orderService.save({
                  issued_at: this.issued_at.format('YYYY-MM-DD'),
                  products: this.products.map( product => { return {id: product.id, product_quantity: product.product_quantity } })
                })
                .pipe(finalize( () => {
                  this.processing_order = false
                  loading.dismiss()
                }))
                .subscribe(
                  next => {
                    this.showOrderStatusAlert('success')
                    this.dismiss()
                  },
                  error => {
                    this.showOrderStatusAlert('error')
                  },
                )
              }
            )
          }
        }
      ]
    })

    await alert.present()
  }
  showOrderStatusAlert(status = 'error') {
    timer(100).subscribe(
      _ => {
        from(
          this.alertController.create({
            cssClass: 'my-custom-class',
            header: `${status == 'success' ? 'Procesado':'Error'}`,
            subHeader: `${status == 'success' ? 'Orden procesada':'No se pudo procesar el pedido'}`,
            message: '',
            buttons: ['OK']
          })
        ).subscribe(
          alert => alert.present()
        )
      }
    )
  }

  async showProcessOrderLoading() {
    const loading = await this.loadingController.create({
      spinner: 'circles',
      duration: 0,
      message: 'Procesando orden',
      translucent: true,
      cssClass: 'custom-class custom-loading',
      backdropDismiss: false,
      animated: true,
    })
    await loading.present()
    return loading
  }

  incProductQuantity(product: any): void{
    if(!product.product_quantity)product.product_quantity = 1
    if(product.product_quantity >= product.prod_stock)return
    product.product_quantity += 1
  }
  decProductQuantity(product: any): void{
    if(!product.product_quantity)product.product_quantity = 1
    if(product.product_quantity > 1) product.product_quantity -= 1
  }

  

  async showRemoveProductAlert(product: any) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Retirar producto?',
      message: `Product: ${product.prod_name}`,
      buttons: [
        {
          text: 'NO',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'SI',
          handler: () => {
            this.products = this.products.filter( pp => pp.id != product.id)
          }
        }
      ]
    })

    await alert.present()
  }
  async previewProductInfoModal(product: any){
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Info producto',
      subHeader: `${product.prod_name}`,
      message: `Código: ${product.prod_barcode}<br />Stock: ${product.prod_stock}<br />Precio Ref: S/. ${product.prod_price}`,
      buttons: ['OK']
    })

    await alert.present()
  }
}
