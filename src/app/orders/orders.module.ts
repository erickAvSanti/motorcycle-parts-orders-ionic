import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrdersPageRoutingModule } from './orders-routing.module';

import { OrdersPage } from './orders.page';
import { ProductSearchComponent } from './product-search/product-search.component';
import { MaterialModule } from '../material.module'
import { OrderAddComponent } from './order-add/order-add.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaterialModule,
    OrdersPageRoutingModule
  ],
  declarations: [OrdersPage, ProductSearchComponent, OrderAddComponent]
})
export class OrdersPageModule {}
