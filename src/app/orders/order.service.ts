import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private defaultHttpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept' : 'application/json'
    })
  }

  currentOrder$: BehaviorSubject<any> = new BehaviorSubject<{}>({})

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private router: Router,
  ) { }
  searchProducts(vv: string): Observable<any>{

    const options = {
      headers: new HttpHeaders({
        'Accept': 'application/json'
      }),
      params: {
        query: vv,
        order_by:'prod_name',
        order:'asc',
        without_pagination: '1',
        ignore_dates: '1',
      }
    }

    return this.http.get<[]>(`${environment.SERVER_URL_API}/products`,options)
  }
  addProductToCurrentOrder(product: any): void{
    this.currentOrder$.next({product})
  }

  save(data: {issued_at: string, products: Array<{id: number, product_quantity: number}>}){
    return this.http.post<any>(`${environment.SERVER_URL_API}/products/order`,data, this.defaultHttpOptions)
  }
  all(): Observable<any>{

    const options = {
      headers: new HttpHeaders({
        'Accept': 'application/json'
      }),
      params: {
        search: '',
        order_by:'',
        order:'',
        without_pagination: '1',
        ignore_dates: '1',
      }
    }

    return this.http.get<[]>(`${environment.SERVER_URL_API}/products/order`,options)
  }

}
