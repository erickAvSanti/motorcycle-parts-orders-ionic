import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { IonRefresher, IonSearchbar, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { from, fromEvent, timer } from 'rxjs';
import { finalize, timeout } from 'rxjs/operators';
import { OrderService } from '../order.service';

@Component({
  selector: 'app-product-search',
  templateUrl: './product-search.component.html',
  styleUrls: ['./product-search.component.scss'],
})
export class ProductSearchComponent implements OnInit, AfterViewInit {

  wait_records = false
  products_info: any = {}
  flag = false
  search_value = ''
  @ViewChild(IonRefresher) ionRefresher: IonRefresher
  @ViewChild(IonSearchbar) ionSearchbar: IonSearchbar
  constructor(
    public modalController: ModalController,
    private orderService: OrderService,
    private storage: Storage,
  ) { }

  ngOnInit() {
  }
  ngAfterViewInit(){

    timer(100).subscribe(
      _ => {
        this.storage.get('orders_product_search_save_query')
        .then( _ => this.search_value = _)
        
        this.wait_records = true
        from(this.storage.get('orders_product_search'))
        .pipe(finalize(() => this.wait_records = false))
        .subscribe(_ => {
          const values = JSON.parse(_)
          values.records.forEach(record => {
            if(typeof record.product_quantity == 'undefined') record.product_quantity = 1
          })
          this.products_info.records = values.records
        })
      }
    )
  }
  saveData(values: any): void {
    this.storage.set('orders_product_search',JSON.stringify(values))
  }
  saveSearchQuery(value: string): void{
    this.storage.set('orders_product_search_save_query',value)
  }

  
  search(value: string): void {
    console.log(value)
    this.saveSearchQuery(value)
    if(this.wait_records)return
    this.wait_records = true
    this.orderService.searchProducts(value)
    .pipe(finalize( () => {
      this.wait_records = false
      this.flag = true
      this.ionRefresher.complete()
    }))
    .subscribe(
      values => {
        values.records.forEach(record => record.product_quantity = 1)
        this.products_info = values
        this.saveData(values)
      },
      _ => console.log(_)
    )
  }
  
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    })
  }

  selectProduct(product: any): void{
    if(product.prod_stock<=0)return
    this.orderService.addProductToCurrentOrder(product)
    this.dismiss()
  }
  doRefresh(event) {
    this.search(this.ionSearchbar.value)
  }

}
